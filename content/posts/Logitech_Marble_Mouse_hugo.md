---
title: "Logitech Marble Mouse"
tags: [ "mouse" ]
---

# Intro


簡單說 好一陣子前買了這個 [Logitech Marble Mouse](http://www.logitech.com/en-us/product/trackman-marble), 但一直很懶, 今天就來設定有關 linux xorg mouse 相關配置

羅技木星軌跡球具有四個按鍵，且是對稱的，因而不論左右手都適合使用。缺點是不具滾輪功能，但可以設定來啟用此功能

# setting

default

| ID  | 硬體動作       | 結果   |
| --- | -------------- | ------ |
| 1   | 左大按鍵       | 左鍵   |
| 2   | 雙擊（大按鍵） | 中鍵 † |
| 3   | 右大按鍵       | 右鍵   |
| 4   | 「無」         | -      |
| 5   | 「無」         | -      |
| 6   | 「無」         | -      |
| 7   | 「無」         | -      |
| 8   | 左小按鍵       | 上一頁 |
| 9   | 右小按鍵       | 下一頁 |


add file `/usr/share/X11/xorg.conf.d/50-marblemouse.conf`

```
Section "InputClass"
    Identifier  "Marble Mouse"
    MatchProduct "Logitech USB Trackball"
    MatchIsPointer "on"
    MatchDevicePath "/dev/input/event*"
    Driver "evdev"
    Option "ButtonMapping" "1 2 3 4 5 6 7 8 9"
    Option "EmulateWheel" "true"
    Option "EmulateWheelButton" "8"
    Option "ZAxisMapping" "4 5"
    Option "XAxisMapping" "6 7"
    Option "Emulate3Buttons" "true"
    Option "EmulateWheelInertia" "10"            # Factory default: 50
EndSection
```

xorg configuration file not take affect util xorg session is restart. To restart xorg session, restart windows manager

in Ubuntu 16.04, using systemd

```
sudo service lightdm restart
```

in Linux Mint 18.1

```
sudo service mdm restart
```

and remember this file will affect windows manager,if configuration cuase error, you need switch to other tty and amend config, then restart the windows manager

# refer
- [Arch wiki marble mouse](https://wiki.archlinux.org/index.php/Logitech_Marble_Mouse)
- [Ubuntu wiki](https://help.ubuntu.com/community/Logitech_Marblemouse_USB)
