---
title: "Socket Programming"
tags: [ "socket"]
---

# example

[python socket doc](https://docs.python.org/3/library/socket.html)


`What's the return value of Socket.accept() in python`

```py
# server.py
import socket

s = socket.socket()
host = socket.gethostname()
port = 1234
s.bind((host, port))

s.listen(5)

while True:
    c, addr = s.accept()
    print 'Got connection from', addr
    c.send('Thank you for your connecting')
    c.close()
```

```
A pair (host, port) is used for the AF_INET address family, where host is a string representing either a hostname in Internet domain notation like 'daring.cwi.nl' or an IPv4 address like '100.50.200.5', and port is an integer.
```

