---
title: "QTS with python3"
tags: ["qts","python3"]
---

# what is QTS?
It is an embeded OS, deloy on NAS, develop by QNAP

[QTS 4.3](https://www.qnap.com/qts/4.3/zh-tw/)

# install Python3
install python3 on QTS 4.3.3

ENV

- TS-453 mini
- QTS 4.3.3.0154

```bash
# cat /proc/version 
Linux version 4.2.8 (root@U16BuildServer56) (gcc version 4.9.2 (toolchain config: [gcc-4.9.2 binutils-2.25 glibc-2.21]) ) #1 SMP Thu Apr 13 12:27:53 CST 2017
```


## install python3 qpkg

1. from qpkg center
2. [python3.5 qpkg](https://download.qnap.com/QPKG/Python3_3.5.2.0_x86_64.zip)

```
sh yourpackage.qpkg
```

## setup


the install package should place in path down below

```
/share/CACHEDEV1_DATA/.qpkg/Python3
```

you can see the readme, how to enter python3 env, but it's not all

```bash
$ . /etc/profile.d/python3.bash
```

it miss the shebang path, you can see on `vim /share/CACHEDEV1_DATA/.qpkg/Python3/src/bin/pip3`

```
#!/usr/local/python3/bin/python3.5
```

the qpkg install script not automatic link qpkg path to execute path



## fixit


```
ln -s /share/CACHEDEV1_DATA/.qpkg/Python3/python3 /usr/local/python3
```

