---
title: "UBoot and BIOS"
date: 2019-05-09
tags: [ "bios", "uboot"]
---

## PC, BIOS

由於BIOS與硬體系統整合在一起（將BIOS程式指令燒錄在IC中），所以有時候也被稱為韌體。在大約1990年BIOS是保存在ROM（唯讀記憶體）中而無法被修改。因為BIOS的大小和複雜程度隨時間不斷增加，而且硬體的更新速度加快，令BIOS也必須不斷更新以支援新硬體，於是BIOS就改為儲存在EEPROM或者快閃記憶體中，讓使用者可以輕易更新BIOS。然而，不適當的執行或是終止BIOS更新可能導致電腦或是裝置無法使用。為了避免BIOS損壞，有些新的主機板有備份的BIOS（「雙BIOS」主機板）。有些BIOS有「啟動區塊」（Boot Block），屬於唯讀記憶體的一部份，一開始就會被執行且無法被更新。這個程式會在執行BIOS前，驗證BIOS其他部分是否正確無誤（經由檢查碼，湊雜碼等等）。如果啟動區塊偵測到主要的BIOS已損壞，通常會自動由軟碟機等裝置啟動電腦，讓使用者可以修復或更新BIOS。一部份主機板會在確定BIOS已損壞後自動搜尋軟碟機等裝置看看有沒有完整的BIOS檔案。此時使用者可以放入儲存BIOS檔案的軟碟（例如由網上下載的更新版BIOS檔案，或是自行備份的BIOS檔案）。啟動區塊會在找到軟碟中儲存的BIOS檔案後自動嘗試更新BIOS，希望以此修復已損壞的部份。硬體製造廠商經常發出BIOS升級來更新他們的產品和修正已知的問題。


1. 載入 BIOS 的硬體資訊與進行自我測試，並依據設定取得第一個可開機的裝置
2. 讀取並執行第一個開機裝置內 MBR 的 boot Loader (亦即是 grub2, spfdisk 等程式)；
3. 依據 boot loader 的設定載入 Kernel ，Kernel 會開始偵測硬體與載入驅動程式；
4. 在硬體驅動成功後，Kernel 會主動呼叫 systemd 程式，並以 default.target 流程開機；
    - systemd 執行 sysinit.target 初始化系統及 basic.target 準備作業系統；
    - systemd 啟動 multi-user.target 下的本機與伺服器服務；
    - systemd 執行 multi-user.target 下的 /etc/rc.d/rc.local 檔案；
    - systemd 執行 multi-user.target 下的 getty.target 及登入服務；
    - systemd 執行 graphical 需要的服務


簡單說在 BIOS 執行硬體初始化後轉交給, GRUB 來做後續開機動作

```
選單一：MBR(grub2) --> kernel file --> booting
選單二：MBR(grub2) --> boot sector(Windows loader) --> Windows kernel --> booting
選單三：MBR(grub2) --> boot sector(grub2) --> kernel file --> booting
```

## Embedded system, bootloader

嵌入式linux中，沒有BIOS，而是直接從flash中運行，來裝載內核。它可以初始化硬件設備，從而將系統的軟硬件環境帶到一個合適的狀態，以便為最終調用操作系統做好準備。

BootLoader-->Boot Parameters-->Kernel-->Root Filesystem

其中最多人用的是 U-boot

```
Boot ROM ---> run U-Boot--> Load Linux kernel(zImage) --> load Device Tree(devicetree.dtb)
---> Load RAM Disk(initrd) --> Run Linux Kernel
```

refer

- [鳥哥 19.1.2 BIOS, boot loader 與 kernel 載入](http://linux.vbird.org/linux_basic/0510osloader.php#process_1)
- [【整理】BIOS、BootLoader、uboot對比](http://work.oknow.org/2016/05/biosbootloaderuboot.html)


