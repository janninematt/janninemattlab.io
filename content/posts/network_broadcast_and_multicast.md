---
title: "Unicast/Broadcast/Multicast 的區別"
tags: [ "Unicast", "Broadcast", "Multicast"]
---

# Unicast


通常指的是特定的目的地位址，一般是主機之間互相傳遞封包的方式，也是最常見的網路通訊方式。
因此我們有時稱之為One-to-One的通訊方式

以Ethernet網路架構而言，封包(Packet)在同一個subnet中傳遞時，以收方位址來判別該由那台主機接收；
若在不同的subnet時，就 要透過路由器Router根據收方位址，把這個packet送往收方主機所在的另一個subnet上。
這就是Internet上最普遍、一對一方式傳 送的unicast。


```
A to B

A 192.168.100.2/24 -> gateway(192.168.100.0) -> packet ip routing

B 172.17.22.100 <- gateway(172.17.22.0) <- routing
```


```
The gateway is a layer 3 device such as a router or multi-layer switch that is used to route traffic on a hop-by-hop basis
```

# Broadcast

通常發生於 MultiAccess (CSMA/CD)  網路媒介中，例如區域網路(Local Area Network)。

- 在Layer 2(Data Link Layer)中表頭的MAC目的地位址通常是 `FF-FF-FF-FF-FF-FF`。
- 在Layer 3(Network Layer) 中表頭的IP目的地位址通常是 `255.255.255.255`。

連接至同一個網段(Segment)網路媒介上的所有主機及網路設備都會接收到這個封包並進行處理。

![](/imgs/osi.jpg)

# Multicast


一般應用於相同的來源資料要同時傳送給一群特定的接收者(Multicast Group Client)，但是來源端只要發送一份資料，因此頻寬的使用量不會因為接收者增加而增加。

Switch must support Multicast

其中Class  D的start bits為"1110"，範圍從224.0.0.0~239.255.255.255，這段位址不屬於任何主機，是特別保留給 multicast  address。當一部主機要送multicast packet到某一個 group的主機時，他們之間要先選定一個閒置的Class D IP，並避免和其他群組的multicast packet IP 相同。
然後這個multicast packet送出時，網路上參與這個 group 的主機，除了接收屬於自己IP的packet之外，也會接收自這個Class D IP 的packet。於是達成了 multicast 的目的了。

# Refer

[Unicast/Broadcast/Multicast 的區別](https://belkin1053.blogspot.tw/2011/12/unicastbroadcastmulticast.html)

