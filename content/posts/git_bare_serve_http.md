---
title: "Git bare serve http"
date: 2019-07-11
tags: [ "git" ]
---

要把舊的 git server repo 搬到現在的 gitea server 上

舊的 git server 是用 ssh 方式做權限管控及access, repo 就手動開 git bare repo
但是 gitea 不支援透過 ssh migrate
gitea 只支援透過 http, https migrate 遠端 repo 到 serer 上

所以現在就回到正題 將 git bare repo export as http static file

## 0. story

舊的 git server repo layout

```
/var/git/repo1
/var/git/repo2
/var/git/repo3
```

一般存取就會是 `ssh://@git{ip}:22/var/git/repo1`


## 1. update server info

首先你要將要 export 的 repo 做 server info 更新, 這樣 git 才能正常透過 http 存取

```sh
#Git 將執行這個 git-update-server-info 命令來更新匿名 HTTP 存取取得資料時所需要的檔案。
cd "${repo_name}"
git --bare update-server-info
```

## 2. serve it

假如有 python 直接拿現成的來用

```sh
cd /var/git/
python -m http.server 8000
# If using python2:
# python -m SimpleHTTPServer 8000
```


不然自己寫一個也可

```go
package main                                                                                                                                                                                                   

import (
        "flag"
        "log"
        "net/http"
)

func main() {
        port := flag.String("port", "80", "port to export")
        path := flag.String("path", "", "path to serve")
        flag.Parse()

        http.Handle("/", http.FileServer(http.Dir(*path)))
        if err := http.ListenAndServe(":"+*port, nil); err != nil {
                log.Fatal(err)
        }
}
```

```sh
go build -o serveThis
./serveThis -p 8000 -path /var/git/*
```

之後可以透過 http 去 access 這些 repo

```sh
git clone http://{ip}:8000/repo1
```

## 3. gitea migrate

新增 migrate repo, 填上我們弄好的 http url 就可以拉

