---
title: "如何 attach ubis partition"
date: 2019-05-09
tags: ["uboot"]
---

假設我 attach 一個 NAND flash

```sh
$ cat /proc/partitions
mtdblock0
mtdblock1
mtdblock2
mtdblock3
mtdblock4
mtdblock5
mtdblock6
```

且確認 block3, block5, block6 有 ubifs


attach 方式

```sh
mkdir -p /mnt/boot_disk
ubiattach /dev/ubi_ctrl -m 3
mount -t ubifs ubi0_0 /mnt/boot_disk/

#do something
umount /mnt/boot_disk
ubidetach /dev/ubi_ctrl -m 3
```


記住: 假如你沒有 umount 則無法 detach
