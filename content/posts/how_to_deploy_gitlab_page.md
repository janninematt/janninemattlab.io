---
title: How to deploy gitlab page with pelican
tags: [ "gitlab", "pelican" ]
---

如何在 gitlab page deploy 靜態網頁 static site


1. push to gitlab
2. gitlab CI phase 1.1: base docker image
3. gitlab CI phase 1.2: base docker pull my pelican build image
4. gitlab CI phase 1.3: base docker mapping source code into my pelican build image
5. gitlab CI phase 2: my pelican build image generate static site
6. finsih generate(確保最後產生 static file 要在 根目錄 `/public` )


## issues

面臨到的問題

1. 我要用 CI base on dokcer image, 再用我自己的 image build, 卻遇到 docker daemon is not running error

    在 `.gitlab-ci.yml` 裡加入 `docker:dind`

    
    docker:dind builds on docker:latest and starts a docker daemon as its entrypoint.

    ```yml
    image: docker:latest
    variables:
        MOUNT_POINT: /builds/$CI_PROJECT_PATH
    services:
        - docker:dind
    ```


2. deploy static site, build test pass but deploy fail

    因為 gitlab page deploy 要將 public 放在根目錄, 這是 gitlab 設計上的決策

    [Make it possible to define custom site directory within artifacts for GitLab Pages](https://gitlab.com/gitlab-org/gitlab-ee/issues/401)


    



