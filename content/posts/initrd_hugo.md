---
title: "initrd repack"
tags: [ "initrd" ]
---

# repack

initrd repack 使用 cpio and xz 來 猜解

1. `. repack_initrd.sh` load function
2. `unpack <initrd_path> <untar_path>` unpack
3. `pack <source_path> <tarball_path>` pack

```bash
function unpack(){
    # we will switch dir path, make sure path store in absolute path
    local initrd_path=$( realpath ${1} )
    local untar_path=$( realpath ${2} )
    # we will create a chroot like dir, we need root permission
    if [ "$EUID" -ne 0 ];then
        echo "Please run as root"; exit 1;
    fi
    # check file and untar path exist
    [ -f ${initrd_path} ] || { echo "the initrd file path is not exist" >&2; exit 1; }
    [ -d ${untar_path} ] || mkdir -p ${untar_path}
    [ ! "$( ls -A ${untar_path} )" ] || { echo "the untar dir is not empty, removing.."; rm ${untar_path}/* -rf;}
    # check file format is correct, xz lzma
    echo "$( file ${initrd_path} )" | grep 'LZMA compressed'
    [ "$?" = "0" ] || { echo "the initrd file:${initrd_path} is not xz format" >&2; exit 1; }
    # untar to path
    cd ${untar_path};
    xzcat -d ${initrd_path} | cpio -idm;
}

function pack(){
    local source_path=$( realpath ${1} )
    local tarball_path=$( realpath ${2} )
    if [ "$EUID" -ne 0 ];then
        echo "Please run as root"; exit 1;
    fi
    [ -d ${source_path} ] || { echo "the initrd path:${source_path} is not exist" >&2; exit 1; }
    [ ! -f ${tarball_path} ] || { echo "${tarball_path} exist, removing...."; rm ${tarball_path}; }
    echo "packing...."
    cd ${source_path};find . | cpio -H newc -o | xz --format=lzma --check=crc32 > ${tarball_path};
}
```
