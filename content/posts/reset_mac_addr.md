---
title: "reset mac addr"
tags: [ "mac", "ifconfig" ]
---

# how
底下 snippet code 為如何重設 mac addr, 需要先關閉再重啟


```bash	
ifconfig eth0 down
ifconfig eth0 hw ether 00:11:22:33:44:55
ifconfig eth0 up
```

假如需要重新向 dhcp 要 ip

1. release the IP address
2. request new IP

```bash	
dhcpcd -k eth0
dhcpcd eth0
```

