---
title: "C get terminal size"
date: 2023-08-13
tags: [ "c"]
---


```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <wordexp.h>

int main()
{
    // get terminal size using ioctl get win size (TIOCGWINSZ) in C
    struct winsize ws;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) < 0) {
        perror("ioctl");
        exit(1);
    }


    printf("lines:%d\n", ws.ws_row);
    printf("columns:%d\n", ws.ws_col);
    return 0;
}
```
