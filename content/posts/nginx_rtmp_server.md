---
title: "setup nginx rtmp server"
tags: [ "nginx", "rtmp" ]
---


# Nginx as a RTMP server

we need to manual compile nginx with addition RTMP module

# refer
# refer

[Setup Nginx-RTMP on Ubuntu 14.04](https://www.vultr.com/docs/setup-nginx-rtmp-on-ubuntu-14-04)

# download
- [nginx](http://nginx.org/en/download.html)
- [nginx-rtmp-module](https://github.com/arut/nginx-rtmp-module/releases)

# env lib setup

```sh
sudo apt install build-essential libpcre3 libpcre3-dev libssl-dev libssl ffmpeg
```

# lazy setup script
```bash
#!/bin/bash
sudo apt install build-essential libpcre3 libpcre3-dev libssl-dev ffmpeg -y

nginx_version="1.12.0"
nginx_rtmp_module_version="1.1.11"

nginx_file="nginx-${nginx_version}.tar.gz"
nginx_url="http://nginx.org/download/${nginx_file}"
nginx_dir_name="nginx-${nginx_version}"

nginx_rtmp_module_url="https://github.com/arut/nginx-rtmp-module/archive/v${nginx_rtmp_module_version}.tar.gz"
nginx_rtmp_module_file="nginx-rtmp-module-${nginx_rtmp_module_version}.tar.gz"
nginx_rtmp_module_dir_name="nginx-rtmp-module-${nginx_rtmp_module_version}"

tmp_dir='nginx_tmp'

mkdir -p ${tmp_dir}
cd ${tmp_dir}
wget ${nginx_url} -O ${nginx_file}
wget ${nginx_rtmp_module_url} -O ${nginx_rtmp_module_file}
tar -xf ${nginx_file}
tar -xf ${nginx_rtmp_module_file}
cd ${nginx_dir_name}
./configure  --prefix=/usr/local/nginx --with-http_ssl_module --add-module=../${nginx_rtmp_module_dir_name}
make && sudo make install
echo 'usage:'
echo 'start: /usr/local/nginx/sbin/nginx'
echo 'end: /usr/local/nginx/sbin/nginx -s [stop|quit]'
```

```
執行：/usr/local/nginx/sbin/nginx
結束：/usr/local/nginx/sbin/nginx -s [stop|quit]
```

