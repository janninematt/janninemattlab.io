---
title: "OS Manage"
tags: [ "os" ]
---

Creating a group

`sudo addgroup groupname`

Creating a user into this group

`sudo adduser username groupname`

Permissions Restriction

See this [thread](http://superuser.com/questions/149404/how-can-i-create-an-ssh-user-who-only-has-permission-to-access-specific-folders) for permissions.
