---
title: "自訂 domain mapping"
date: 2019-05-09
tags: [ "DNS","domain" ]
---

公司內部有 DNS server, 且有自訂幾個 private domain 指向
覺的 DNS server response 有點慢, 但是幾個重要的 domain 又不能丟
乾脆將那幾個 private domain 寫在 hosts 檔案上, 這樣我換 dns 也不會影響到他的 mapping

這個方法是依據 `/etc/nsswitch.conf` 查詢 host 的順序達成的

```
hosts:          files mdns4_minimal [NOTFOUND=return] dns
```

1. Begin by checking the /etc/Hosts file. If that file provides an IP address for the host name in question, it is used.
2. Otherwise try mdns4_minimal, which will attempt to resolve the name via multicast DNS only if it ends with .local. If it does but no such mDNS host is located, mdns4_minimal will return NOTFOUND. The default name service switch response to NOTFOUND would be to try the next listed service, but the [NOTFOUND=return] entry overrides that and stops the search with the name unresolved.
3. Then try the specified DNS servers. This will happen more-or-less immediately if the name does not end in .local, or not at all if it does. If you remove the [NOTFOUND=return] entry, nsswitch would try to locate unresolved .local hosts via unicast DNS. This would generally be a bad thing , as it would send many such requests to Internet DNS servers that would never resolve them. Apparently, that happens a lot.
4. The final mdns4 entry indicates mDNS will be tried for names that don't end in .local if your specified DNS servers aren't able to resolve them. I thought this was meant to catch mDNS hosts when you don't specifiy the .local TLD, but I just tried it and it doesn't work. Guess I will look into it.


```
nsswitch.conf - Name Service Switch configuration file
The Name Service Switch (NSS) configuration file, /etc/nsswitch.conf,
is used by the GNU C Library and certain other applications to
determine the sources from which to obtain name-service information
in a range of categories, and in what order.  Each category of
information is identified by a database name.

The file is plain ASCII text, with columns separated by spaces or tab
characters.  The first column specifies the database name.  The
remaining columns describe the order of sources to query and a
limited set of actions that can be performed by lookup result.
```


1. 查詢自己目前 dns server 設定(要先是目標 dns 的)

    透過 DHCP 配給 dns serve, 所以要查一下

    ```
    $ nmcli dev show | grep 'DNS'
    IP4.DNS[1]:                             1.2.3.4
    ```

2. 用該 dns 查詢目標 domain

    ```
    $ dig eat.somedomain.com
    eat.somedomain.com. 3600 IN A 192.168.1.2
    ```

    這樣我知道該 domain 是透過 A record 去做 forward

3. 寫在 hosts file 中

    ```
    $ sudo vim /etc/hosts

    # [IP address]  [URL]                                                                                                                                                                     
    127.0.0.1       localhost
    127.0.1.1       myhost
    
    # internal only mapping
    192.168.1.2    eat.somedomain.com

    # The following lines are desirable for IPv6 capable hosts
    ::1     ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    ```

4. 切換你自己想要的 DNS server, 並確認原本的 domain 仍可 access

這樣假如 browser 要去 lookup `eat.somedomain.com`, 馬上會從 local file 得到 resolve
而不會從我之後設定的 dns server 取得
