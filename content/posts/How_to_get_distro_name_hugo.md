---
title: "How to get distro name"
tags: [ "distro","QTS","bash" ]
---

# snippet

```bash
#!/bin/bash

function getDistro {
    if [ -f /usr/bin/lsb_release ]; then
        lsb_release -si
    elif [ -f /etc/os-release ]; then
        source /etc/os-release
        if [ -n "${PRETTY_NAME}" ]; then
            echo "${PRETTY_NAME}"
        else
            echo "${NAME}"
        fi
    # now looking at distro-specific files
    elif [ -f /etc/arch-release ]; then
        echo "Arch Linux"
    elif [ -f /etc/gentoo-release ]; then
        cat /etc/gentoo-release
    elif [ -f /etc/fedora-release ]; then
        cat /etc/fedora-release
    elif [ -f /etc/redhat-release ]; then
        cat /etc/redhat-release
    elif [ -f /etc/debian_version ]; then
        echo "Debian GNU/Linux " ; cat /etc/debian_version
    else
        echo "Unknown"
    fi
}

function isQTS {
    local distro=$1
    if [[ "${distro}" == QTS* ]]; then
        echo 0
    else
        echo 1
    fi
}

distro=$(getDistro)

if [ "0" == "$(isQTS ${distro})" ]; then

    echo "do something"
else
    echo "do something else"
fi
```

