---
title: "Machine Learning Intro"
tags: [ "ml" ]
---

## Intro

- supervised learning
    - know the answer of the problem
    - letting the computer work out that relationship for you
- unsupervised learning
    
    ```
    This is kind of like someone giving you a list of numbers on a sheet of paper
    and saying “I don’t really know what these numbers mean but maybe you can figure
    out if there is a pattern or grouping or something — good luck!”
    ```

    - unknow the answer of the problem

[machine learning wiki](https://en.wikipedia.org/wiki/Machine_learning#Algorithm_types)

- Supervised learning: The computer is presented with example inputs and their desired outputs, given by a "teacher", and the goal is to learn a general rule that maps inputs to outputs.
- Unsupervised learning: No labels are given to the learning algorithm, leaving it on its own to find structure in its input. Unsupervised learning can be a goal in itself (discovering hidden patterns in data) or a means towards an end (feature learning).
- Reinforcement learning: A computer program interacts with a dynamic environment in which it must perform a certain goal (such as driving a vehicle), without a teacher explicitly telling it whether it has come close to its goal. Another example is learning to play a game by playing against an opponent

quote

```
Maybe a better definition for “learning” in this case is “figuring out an equation to solve a specific problem based on some example data”.
```

multivariate linear regression 多元線性迴歸 linear data

step

1. set each weight to 1
2. evaluate the see the cost
3. tune each weight make the cost as small as possible, repeat step 2


梯度下降法
[batch gradient descent](https://hbfs.wordpress.com/2012/04/24/introduction-to-gradient-descent/)

if dataset is big, it will run the many time of dataset

随机梯度下降法
stochastic gradient descent SGD

random pick sample data, then use gradient decent, repeat


non-linear data(neural networks or SVMs with kernels)

`overfitting`


遞歸神經網絡
Recurrent Neural Network keep track state of model, that can predict more base on past data

[face recognition with deep learning](https://medium.com/@ageitgey/machine-learning-is-fun-part-4-modern-face-recognition-with-deep-learning-c3cffc121d78#.c6dw9vj57)

卷積神經網絡
Deep convolutional neural networks

Our image processing pipeline is a series of steps: convolution, max-pooling, and finally a fully-connected network.

1. Break the image into overlapping image tiles
2. Feed each image tile into a small neural network
3. Save the results from each tile into a new array
4. Downsampling
5. Make a prediction

1. Finding all the Faces
    break up the image into small squares of 16x16 pixels each
    HOG image
    
2. Posing and Projecting Faces

face landmark estimation [paper](http://www.csc.kth.se/~vahidk/papers/KazemiCVPR14.pdf)

The basic idea is we will come up with 68 specific points (called landmarks) that exist on every face,
 the top of the chin, the outside edge of each eye, the inner edge of each eyebrow, etc.
Then we will train a machine learning algorithm to be able to find these 68 specific points on any face

dlib shape_predictor_68_face_landmarks, to catch the face

![](/imgs/obama.png)

![](/imgs/obama_face.png)


1. Encoding Faces

chanllenge

```
The simplest approach to face recognition is to directly compare the unknown face we found in Step 2 with all the pictures we have of people that have already been tagged. When we find a previously tagged face that looks very similar to our unknown face, it must be the same person. Seems like a pretty good idea, right?

There’s actually a huge problem with that approach. A site like Facebook with billions of users and a trillion photos can’t possibly loop through every previous-tagged face to compare it to every newly uploaded picture. That would take way too long. They need to be able to recognize faces in milliseconds, not hours.

```

use Deep Convolutional Neural Network

But instead of training the network to recognize pictures objects like we did last time, we are going to train it to generate 128 measurements for each face.

The training process works by looking at 3 face images at a time:

1. Load a training face image of a known person
2. Load another picture of the same known person
3. Load a picture of a totally different person



python ./align-dlib.py ./training-images/ align outerEyesAndNose ./aligned-images/ --size 96

lua5.1

luarocks install csvigo
luarocks install dpnn



According to OpenCV documentation, in OpenCV depth is defined as the bit-depth of an individual channel. So if you have 8 bit depth and 3 channels, it means you have 24 bits per image pixel
[dlib](https://github.com/davisking/dlib)

[python dlib doc](http://dlib.net/python/#)

A toolkit for making real world machine learning and data analysis applications


boost

```
sudo apt-get install libboost-all-dev
```

upgrade cmake > 2.8

```
sudo add-apt-repository ppa:george-edison55/cmake-3.x
sudo apt-get update
sudo apt-get install cmake
```

final

```
pip install dlib
```

it may take few minutes to complish.

refer [How to install dlib for python3 in Ubuntu 14.04](http://askubuntu.com/questions/824834/how-to-install-dlib-for-python3-in-ubuntu-14-04)

it also work in virtualenv

[openface]()
[facenet](https://github.com/davidsandberg/facenet)




download shape_predictor_68_face_landmarks.dat file from:
[shape_predictor_68_face_landmarks](http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2)


[scikit image](http://scikit-image.org/docs/dev/install.html)

```
pip install scikit-image
```


Multi-Task Deep Convolutional Neural Network (MTCNN)

```bash
<path to python package>/openface/torch_neural_net.py

python3 string not support buffer, need to encoding

write(byte('what', 'UTF-8'))

read().decode('UTF-8')
```


also picke dump or load using binary file

with open`('my_dumped_classifier.pkl', 'rb')` as fid: like this one


sklearn

and [LabelEncoder](http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html) accept list or tuple, not map

convert it

```python
>>> from sklearn import preprocessing
>>> le = preprocessing.LabelEncoder()
>>> le.fit([1, 2, 2, 6])
LabelEncoder()
>>> le.classes_
array([1, 2, 6])
>>> le.transform([1, 1, 2, 6]) 
array([0, 0, 1, 2]...)
>>> le.inverse_transform([0, 0, 1, 2])
array([1, 1, 2, 6])
```


[berkeleyvision model](http://caffe.berkeleyvision.org/model_zoo.html)
[五種開源授權規範的比較 (BSD, Apache, GPL, LGPL, MIT)](http://inspiregate.com/internet/trends/74-comparison-of-five-kinds-of-standard-open-source-license-bsd-apache-gpl-lgpl-mit.html)

[那些台灣軟體產業所缺少的 – 開放源始碼](http://blog.ez2learn.com/2011/11/25/taiwan-software-lacking-of-open-source/)

http://deeplearning.net/datasets/

https://github.com/davidsandberg/facenet

http://cmusatyalab.github.io/openface/setup/

http://vis-www.cs.umass.edu/lfw/#explore
[极客学院 Wiki](http://wiki.jikexueyuan.com/project/tensorflow-zh/tutorials/mnist_tf.html)

[Machine Learning is Fun! Part 3: Deep Learning and Convolutional Neural Networks](https://medium.com/@ageitgey/machine-learning-is-fun-part-3-deep-learning-and-convolutional-neural-networks-f40359318721#.1mbowv69z)

[ 人脸识别：Deep Face Recognition论文阅读](http://yongyuan.name/blog/deep-face-recognition-note.html)

[udacity-deep-learning](https://github.com/Arn-O/udacity-deep-learning)

