---
title: "Change CPU clockrate"
date: 2019-05-27
tags: [ "cpu" ]
---

如何更改 CPU clock rate


ELI5: What is the clock speed of a CPU?

> CPUs run one operation per cycle of the internal clock. A higher clock speed means the CPU operates more operations in a given amount of time, 
> so it performs faster. A higher clock cycle generates more heat though, so people who overclock (manually speed up the PLC clock using software)
> their CPUs need aftermarket cooling to maintain good CPU temps.


幾個東西可查看

- cpuinfo_min_freq : CPU 可以調整的最低頻率
- cpuinfo_max_freq : CPU 可以調整的最高頻率

幾個比較重要的東西可以調

- scaling_min_freq : CPU 運作下的最低頻率
- scaling_max_freq : CPU 運作下的最高頻率


所以你可以藉由調整的上述兩個 upper bound 和 lower bound ，來設定 CPU 頻率的運作時候的範圍調法如下


底下可看出目前 clockrate, 單位為 `KHz`

- 最低的 cpu clockrate `800MHz`
- 最高的 cpu clockrate `4000MHz` `4GHz`
  

```sh
$ cat /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq
800000

$ cat /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq 
4000000
```


```sh
$ # echo {欲調整的頻率} > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
$ echo 2000000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
```


## 選擇適當的調節器


調節器即Governor，用它來控制CPU頻率。可選的有


| 內核模塊             | 調節器名稱   | 功能                                                           |
| -------------------- | ------------ | -------------------------------------------------------------- |
| cpufreq_ondemand     | ondemand     | 按需調節，內核提供的功能，不是很強大，但有效實現了動態頻率調節 |
| cpufreq_powersave    | powersave    | 省電模式，通常以最低頻率運行，                                 |
| cpufreq_userspace    | userspace    | 用戶模式，一些調頻工具軟體需要在此模式下才能運行               |
| cpufreq_conservative | conservative | 「保守」模式，類似於ondemand，但調整相對較緩                   |
| N/A                  | performance  | 不降頻                                                         |

查看當前的調節器：

```
$ cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
```

更改使用的調節器，需要載入相應的內核模塊，再更改scaling_governor文件，例如：

```
$ modprobe cpufreq_conservative
$ echo conservative > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
```


全部切成 performance, 不降頻

```
for x in /sys/devices/system/cpu/cpu[0-9]/cpufreq/;do    echo "performance" > $x/scaling_governor; done
```
