---
title: "learn git"
tags: [ "git" ]
---

this post base refer [Ry’s Git Tutorial](http://rypress.com/tutorials/git/index)

Git is a VCS(Version Control System), that is "Distributed"

Git repository 3 main components:

1. the working directory
2. the staged snapshot
3. committed snapshots




## Common

- `git init` :create a git repo in current folder, to show out, `ls -al`
- `git status` :view the status of each file in a repository.
- `git add <file>` :add file to stage(wait to commit)
- `git commit` :commit to repo, using default editor to edit message
    - `git commit -m "<message>"` :directly cmd line to endter commit message
    - `git commit -a -m "<message>"` :Stage all tracked files and commit the snapshot using the specified message.
    - `git commit --amend` :Add staged changes to the most recent commit instead of creating a new one
- `git log` :show the commit history
    - `git log --oneline` :show all history, but each commit only one line
    - `git log --oneline <file>` :only show the specified file, git history in one line style
- `git revert <commit-id>` :Undo the specified commit by applying a new commit.
- `git tag -a <tag-name> -m "<description>"` create an annotated tag pointing to the most recent commit.
- `git checkout <commit-id>` :checkout to the repo version
    - `git checkout <tag_name>` :checkout the tag commit
    - `git checkout <branch_name>` :checkout to the branch
- `git reset --hard` :Reset tracked files to match the most recent commit.
    - `git reset` :clean the stage
    - `git reset <file>` :clean the file in the stage
- `git clean -f` :Remove untracked files.
- `git reset --hard / git clean -f` :Permanently undo uncommitted changes.
- `git branch` :View Existing Branches
    - `git branch <branch-name>` :Create a new branch using the current working directory as its base.
    - `git checkout <branch-name>` :Make the working directory and the HEAD match the specified branch.
    - `git branch -d <branch-name>` :Delete a branch, that already merged
    - `git branch -D <branch-name>` :Force the removal of an unmerged branch (be careful: it will be lost forever).
- `git merge <branch-name>` :Merge a branch into the checked-out branch.
    - `git merge --no-ff <branch-name>` Force a merge commit even if Git could do a fast-forward merge, will has a merge commit
- `git rm <file>` :Remove a file from the working directory (if applicable) and stop tracking the file.
- `git mv <file>` :Rename or move the file
- `git rebase <new-base>` :Move the current branch’s commits to the tip of `<new-base>`, which can be either a `branch name` or a `commit ID`.
    - `git rebase -i <new-base>` :Perform an interactive rebase and select actions for each commit.
    - `git rebase --continue` :Continue a rebase after amending a commit.
    - `git rebase --abort` Abandon the current interactive rebase and return the repository to its former state.
- `git reflog` :Display the local, chronological history of a repository.
- `git reset --mixed HEAD~<n>` Move the HEAD backward <n> commits, but don’t change the working directory.
- `git reset --hard HEAD~<n>` :Move the HEAD backward <n> commits, and change the working directory to match.
- `git log <since>..<until>` :Display the commits reachable from <until> but not from <since>. These parameters can be either commit ID’s or branch names.
- `git log --stat` :Include extra information about altered files in the log output.
## Note

1. nerver commit on a middle of HEAD to nowhere branch

you must checkout to master, or just create a new branch then checkout to it
unless you are using `rebase`, it's call `dangling commit` that cannot be reached from any branch and are thus in danger of being lost forever
but the rebase will know will to go

2. branch

- `topic branches` :exist to develop a certain topic, then they are deleted
    - `feature branch` :`longer-running type of topic branch`
        it was created with the intention of developing a specific feature,

- `hotfix branch` :create and test the news updates
     In contrast to our relatively long-running feature branch
- rule-of-thumb
    - Create a new branch for each major addition to your project.
    - Don’t create a branch if you can’t give it a specific name.

3. merge

- `fast-forward merge`, has common history, and no conflict
- `3-way merge` may occur `merge conflict`

conflict will like below

```git
<<<<<<< HEAD
<li><a href="news-1.html">Blue Is The New Hue</a></li>
=======
<li><a href="rainbow.html">Our New Rainbow</a></li>
>>>>>>> crazy
```

- `<<<<<<< HEAD` shows us the version in the current branch
- `after the =======` shows the version in the `crazy branch`.

list merge conflict file

```sh
git diff --name-only --diff-filter=U
```

4. splite or rearrange commit

```sh
git rebase -i <new-base>
```
