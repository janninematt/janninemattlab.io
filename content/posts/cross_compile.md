---
title: "Cross Compiler"
tags: [ "cross_compiler" ]
---

refer [Cross Compiler](http://ryan0988.pixnet.net/blog/post/171244470-%E4%BB%80%E9%BA%BC%E6%98%AFcross-compiler-%3F)

從工作以來就一直使用`Cross Compiler`
可是現在終於知道為何這樣稱呼
我試著用口語來解釋
當我們使用 `compiler` 來編譯某個程式
該程式一定只能執行在與你編譯的機器相同的平台
但是現在`Cross Compiler`編譯出來的不是相同平台
而是要執行在你的`target`平台上，例如`ARM`
所以`Cross Compiler for ARM`就是編譯出來的程式
`得由ARM CPU才能執行`

那為什麼要這麼做呢
由於`Compiler`是很大的程式
需要快速運算以及較多的資源
如果由比較慢的原生平台(如`ARM`)來編譯
那程式設計師不就要常常打瞌睡了
讓快速的`x86 PC`來擔任`編譯ARM程式`會較有效率



預設的

- `include path` 可以透過 `arm-linux-cpp -v` 來取得
- `library path` 是由 `arm-linux-gcc -print-search-dirs` 取得



`LIBRARY_PATH` 和 `LD_LIBRARY_PATH` 環境變量的區別

`LIBRARY_PATH`和`LD_LIBRARY_PATH`是`Linux`下的兩個環境變量

- `LIBRARY_PATH` 環境變量用於在程序編譯期間查找動態鏈接庫時指定查找共享庫的路徑
    例如，指定`gcc`編譯需要用到的動態鏈接庫的目錄。設置方法如下（其中，`LIBDIR1`和`LIBDIR2`為兩個庫目錄）：

    ```sh
    export LIBRARY_PATH=LIBDIR1:LIBDIR2:$LD_LIBRARY_PATH
    ```

- `LD_LIBRARY_PATH` 環境變量用於在程序加載運行期間查找動態鏈接庫時指定除了系統默認路徑之外的其他路徑

    注意， `LD_LIBRARY_PATH` 中指定的路徑會在系統默認路徑之前進行查找。設置方法如下（其中， `LIBDIR1` 和 `LIBDIR2` 為兩個庫目錄）

    ```sh
    export LD_LIBRARY_PATH=LIBDIR1:LIBDIR2:$LD_LIBRARY_PATH
    ```

    舉個例子，我們開發一個程序，經常會需要使用某個或某些動態鏈接庫，為了保證程序的可移植性，可以先將這些編譯好的動態鏈接庫放在自己指定的目錄下，然後按照上述方式將這些目錄加入到 `LD_LIBRARY_PATH` 環境變量中，這樣自己的程序就可以動態鏈接後加載庫文件運行了


- 開發時，設置 `LIBRARY_PATH` ，以便gcc能夠找到編譯時需要的動態鏈接庫。
- 發佈時，設置 `LD_LIBRARY_PATH`，以便程序加載運行時能夠自動找到需要的動態鏈接庫。


also see [What is the difference between LD_LIBRARY_PATH and -L at link time?](http://stackoverflow.com/questions/1904990/what-is-the-difference-between-ld-library-path-and-l-at-link-time)

The settings of LD_LIBRARY_PATH has the highest precedence, so when it is set, the set of directories mentioned by `LD_LIBRARY_PATH` are searched first even before the standard set of directories. So in your case setting of `LD_LIBRARY_PATH` is influencing the lookup of
the libraries mentioned with `-l` option. Without `LD_LIBRARY_PATH` some of the dependencies
might have been resolved from the standard set of directories


