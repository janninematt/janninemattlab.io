---
title: "Gtest install"
date: 2019-12-25
tags: [ "gtest" ]
---


googletest lib install

```sh
#!/bin/bash
requires="libgtest-dev cmake"
prefix="/usr/local"

gtest_version="1.10.0"
gtest_filepath="/tmp/gtest.tar.gz"
build_tmp_dir="/tmp/gtest_tmp"

aptinstall() {
        local package_name=$1
        local ret=0
        dpkg -s ${package_name} >/dev/null
        ret=$?
        if [ $? -ne 0 ]; then
                echo "install ${package_name}..."
                apt install ${package_name}
        fi
}

for package in ${requires}; do
        aptinstall ${package}
done

if [ ! -f "/tmp/${gtest_filename}" ]; then
        echo "download gtest source..."
        wget https://github.com/google/googletest/archive/release-${gtest_version}.tar.gz -O "${gtest_filepath}"
fi

if [ ! -d ${build_tmp_dir} ]; then
        echo "create tmp dir for gtest"
        mkdir ${build_tmp_dir}
fi

echo "extract"
tar -xf "${gtest_filepath}" -C ${build_tmp_dir} --strip-components 1

# check build dir exist
if [ ! -d "${build_tmp_dir}/build" ]; then
        mkdir -p "${build_tmp_dir}/build"
fi

echo "build and install"
cd "${build_tmp_dir}/build" && cmake ../ -DCMAKE_INSTALL_PREFIX=${prefix} && sudo make install -j$(nproc)
```
