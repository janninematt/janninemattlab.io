---
title: "X11 screensavor"
date: 2020-08-21
tags: [ "X11","linux"]
---

簡易 example to start x11 screensavor

```cpp

// g++ main.cpp -lX11 -lXss
#include <stdio.h>
#include <X11/X.h>
#include <X11/extensions/scrnsaver.h>

enum SCREEN_SAVOR_STATE : int {
        E_SCREEN_SAVOR_WAKE = 0,
        E_SCREEN_SAVOR_SLEEP = 1,
        E_SCREEN_SAVOR_DISABLE = 4,
};

int main(int argc, char const *argv[])
{
        Display *dpy = XOpenDisplay(nullptr);

        // A timeout of 0 disables the screen saver
        // nonzero value enables the screen saver
        int timeout_sec = 60;
        int using_server_default = -1;

        /*
        #define DontPreferBlanking	0
        #define PreferBlanking		1
        #define DefaultBlanking		2

        #define DisableScreenSaver	0
        #define DisableScreenInterval	0

        #define DontAllowExposures	0
        #define AllowExposures		1
        #define DefaultExposures	2
        */

        XSetScreenSaver(dpy, timeout_sec, using_server_default, DefaultBlanking,
                        DefaultExposures);

        XScreenSaverInfo *info = XScreenSaverAllocInfo();

        XScreenSaverQueryInfo(dpy, DefaultRootWindow(dpy), info);
        // wake mode state 0
        // sleep mode state 1
        // disable mode state 3
        printf("state:%d\n", info->state);
        switch (info->state) {
        case E_SCREEN_SAVOR_WAKE:
                break;
        case E_SCREEN_SAVOR_SLEEP:
                break;
        case E_SCREEN_SAVOR_DISABLE:
                break;
        default:
                break;
        }

        printf("Idle millisecond:%lu ms\n", info->idle);
        printf("Remain millisecond%lu\n", info->til_or_since);
        XFree(info);
        return 0;
}
```
