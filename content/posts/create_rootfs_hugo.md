---
title: "linux rootfs 製作"
date: 2019-05-09
tags: ["rootfs","linux"]
---

## 簡易 rootfs 製作


```sh
mkdir -p tmp/bin
cd tmp
cp /bin/busybox bin/
sudo chroot . /bin/busybox sh                                                                                       Thu 08 Mar 2018 08:45:56 PM CST

BusyBox v1.22.1 (Ubuntu 1:1.22.0-15ubuntu1) built-in shell (ash)
Enter 'help' for a list of built-in commands.

/ # ls
bin
/ # mkdir proc sys tmp dev etc
/ # ls
bin   dev   etc   proc  sys   tmp
/ # touch init
/ # chmod 755 init
/ # echo "sh" >> init
/ # echo "restart -f" >> init
/ # /bin/busybox --install -s /bin
```

假如想走正規的啟動方式要參考 `/sbin/init` 做的類 systemV 的啟動方式

NOTE: 還有 init mod 要注意至少要能夠執行, 及不能死掉, 否則 kernel 會噴 kernel panic

check [/SBIN/INIT 程式](https://access.redhat.com/documentation/zh-tw/red_hat_enterprise_linux/6/html/installation_guide/s2-boot-init-shutdown-init)


Here is a reasonable minimum set of directories for your root filesystem

```
/dev -- Device files, required to perform I/O
/proc -- Directory stub required by the proc filesystem
/etc -- System configuration files
/sbin -- Critical system binaries
/bin -- Essential binaries considered part of the system
/lib -- Shared libraries to provide run-time support
/mnt -- A mount point for maintenance on other disks
/usr -- Additional utilities and applications
```



## devfs、sysfs、udev介绍

此小節 refer [devfs、sysfs、udev介绍](http://rodney2009.pixnet.net/blog/post/12547567-devfs%E3%80%81sysfs%E3%80%81udev%E4%BB%8B%E7%BB%8D)


linux下有專門的檔系統用來對設備進行管理，devfs和sysfs就是其中兩種。

### devfs

在2.6內核以前一直使用的是devfs，devfs掛載於/dev目錄下，提供了一種類似於檔案的方法來管理位於/dev目錄下的所有設備，
我們知道/dev目錄下的每一個檔案都對應的是一個設備，至於當前該設備存在與否先且不論，
而且這些特殊檔是位於根檔系統上的，在製作檔案系統的時候我們就已經建立了這些設備檔，因此通過操作這些特殊檔，
可以實現與內核進行交互。但是devfs檔案系統有一些缺點，例如：不確定的設備映射，有時一個設備映射的設備檔可能不同，
例如我的U盤可能對應sda有可能對應sdb；沒有足夠的主/輔設備號，當設備過多的時候，顯然這會成為一個問題；
/dev目錄下檔太多而且不能表示當前系統上的實際設備；命名不夠靈活，不能任意指定等等。


### sysfs

正因為上述這些問題的存在，在linux2.6內核以後，引入了一個新的檔案系統sysfs，它掛載於/sys目錄下，
跟devfs一樣它也是一個虛擬檔案系統，也是用來對系統的設備進行管理的，它把實際連接到系統上的設備和匯流排組織成一個分階層的檔案目錄，
用戶空間的程式同樣可以利用這些資訊以實現和內核的交互，該檔案系統是當前系統上實際設備樹的一個直觀反應，
它是通過kobject子系統來建立這個資訊的，當一個kobject被創建的時候，對應的檔案和目錄也就被創建了，
位於/sys下的相關目錄下，既然每個設備在sysfs中都有唯一對應的目錄，那麼也就可以被用戶空間讀寫了。
用戶空間的工具udev就是利用了sysfs提供的資訊來實現所有devfs的功能的，但不同的是，udev運行在用戶空間中，
而devfs卻運行在內核空間，而且udev不存在devfs那些先天的缺陷。很顯然，sysfs將是未來發展的方向。

### udev

udev是一種工具，它能夠根據系統中的硬體設備的狀況動態更新設備檔，
包括設備檔的創建，刪除等。設備檔通常放在/dev目錄下，
使用udev後,在/dev下面只包含系統中真實存在的設備。它於硬體平臺無關的，
位於用戶空間，需要內核sysfs和tmpfs的支援，sysfs為udev提供設備入口和uevent通道，tmpfs為udev設備檔提供存放空間。

### mounting


```sh
mount -t sysfs sys /sys
mount -t proc proc /proc
mount -t tmpfs tmp /tmp
mount -o bind /dev /dev/
```


## UNPACK

- xz

    ```
    $ xz -dc /boot/mini_my_root.boot | cpio -idmv
    $ #using xz -d or xz -dc
    ```

- gzip

    ```
    zcat  /boot/mini_my_root.boot | cpio -idmv
    ```

## PACK

pack 的時候要注意 kernel support 的規格

- xz

    ```
    find . | cpio -H newc -o | xz --check=crc32 > /boot/mini_my_root.boot
    ```

- gzip

    ```
    find . | cpio -H newc -o | gzip --check=crc32 > /boot/mini_my_root.boot
    ```



## 命令參數

### xz

- `-d, --decompress`

    Decompress

- `-c, --stdout, --to-stdout`

    Write the compressed or decompressed data to standard output instead of a file. This implies --keep.

- `-C check, --check=check`

    Specify the type of the integrity check;The check is calculated from the uncompressed data and stored in the .xz file. This option has an effect only when compressing into the .xz format; the .lzma format doesn't support integrity checks. The integrity check (if any) is verified when the .xz file is decompressed.
    Supported check types:

    1. none

        Don't calculate an integrity check at all. This is usually a bad idea. This can be useful when integrity of the data is verified by other means anyway.
    
    2. crc32

        Calculate CRC32 using the polynomial from IEEE-802.3 (Ethernet).
    
    3. crc64

        Calculate CRC64 using the polynomial from ECMA-182. This is the default, since it is slightly better than CRC32 at detecting damaged files and the speed difference is negligible.
    
    4. sha256

        Calculate SHA-256. This is somewhat slower than CRC32 and CRC64.

- `-F format, --format=format`

    Specify the file format to compress or decompress:
    
    1. auto

        This is the default. When compressing, auto is equivalent to xz. When decompressing, the format of the input file is autodetected. Note that raw streams (created with --format=raw) cannot be autodetected.
    
    2. xz

        Compress to the .xz file format, or accept only .xz files when decompressing.
    
    3. lzma or alone

        Compress to the legacy .lzma file format, or accept only .lzma files when decompressing. The alternative name alone is provided for backwards compatibility with LZMA Utils.
    
    4. raw
    
        Compress or uncompress a raw stream (no headers). This is meant for advanced users only. To decode raw streams, you need to set not only --format=raw but also specify the filter chain, which would normally be stored in the container format headers.



### cpio

- `-v, --verbose`

    Verbosely list the files processed.

- `-d, --make-directories`

    Create leading directories where needed.

- `-m, --preserve-modification-time`

    Retain previous file modification times when creating files.

- `-i, --extract`

    Run in copy-in mode.

- `-o, --create`

    Run in copy-out mode.

- `-c`

    Identical to '-H newc', use the new (SVR4 ) portable format. If you wish the old portable (ASCII ) archive format, use '-H odc' instead.

- `-H format, --format=format`


    Use given archive format.

    The valid formats are listed below with file size limits for individual files in parentheses; the same names are also recognized in all-caps.
    The default in copy-in mode is to automatically detect the archive format, and in copy-out mode is ‘bin’.

    1. ‘bin’

        The obsolete binary format. (2147483647 bytes)

    2. ‘odc’

        The old (POSIX.1) portable format. (8589934591 bytes)

    3. ‘newc’

        The new (SVR4) portable format, which supports file systems having more than 65536 i-nodes. (4294967295 bytes)

    4. ‘crc’

        The new (SVR4) portable format with a checksum added.

    5. ‘tar’

        The old tar format. (8589934591 bytes)

    6. ‘ustar’

        The POSIX.1 tar format. Also recognizes GNU tar archives, which are similar but not identical. (8589934591 bytes)

    7. ‘hpbin’

        The obsolete binary format used by HPUX’s cpio (which stores device files differently).

    8. ‘hpodc’

        The portable format used by HPUX’s cpio (which stores device files differently).


refer 

- [xz(1) - Linux man page](https://linux.die.net/man/1/xz)
- [devfs、sysfs、udev介绍](http://rodney2009.pixnet.net/blog/post/12547567-devfs%E3%80%81sysfs%E3%80%81udev%E4%BB%8B%E7%BB%8D)
- [The Linux Bootdisk HOWTO: Building a root filesystem](http://www.tldp.org/HOWTO/Bootdisk-HOWTO/buildroot.html)
