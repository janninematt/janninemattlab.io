---
title: "iptables port forward"
tags: [ "port forward", "iptables" ]
---

# refer

[鳥哥的 Linux 私房菜- 防火牆與 NAT 伺服器](http://linux.vbird.org/linux_server/0250simple_firewall.php)

# sample

```bash
SCRIPT=`busybox realpath $0`
SCRIPTPATH=`dirname $SCRIPT`

rmmod module_name 2>/dev/null
insmod "${SCRIPTPATH}/module.ko" 2>/dev/null

ifconfig eth6 up
ifconfig eth7 up

# wait network.sh dhcp first, then change default interface setting
sleep 5

kill `ps |grep dhcpcd|grep eth6|awk '{print $1}'`
kill `ps |grep dhcpcd|grep eth7|awk '{print $1}'`

ifconfig eth6 192.168.1.1 netmask 255.255.255.0 up
ifconfig eth7 192.168.2.1 netmask 255.255.255.0 up


echo "1" > /proc/sys/net/ipv4/ip_forward
# reset iptable
iptables -t nat -F
iptables -t nat -X
iptables -t nat -F POSTROUTING
iptables -t nat -F PREROUTING
# setup forward
iptables -t nat -A POSTROUTING -s 192.168.1.0/24 ! -o eth6 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 192.168.2.0/24 ! -o eth7 -j MASQUERADE

iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 8000 -j DNAT --to-destination 192.168.1.2:8000
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 8100 -j DNAT --to-destination 192.168.1.2:8100
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 1936 -j DNAT --to-destination 192.168.1.2:1935
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 8300 -j DNAT --to-destination 192.168.1.2:8300
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 9000 -j DNAT --to-destination 192.168.1.2:9000
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 10080 -j DNAT --to-destination 192.168.1.2:8080
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 10022 -j DNAT --to-destination 192.168.1.2:22

iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 8001 -j DNAT --to-destination 192.168.2.2:8000
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 8101 -j DNAT --to-destination 192.168.2.2:8100
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 1937 -j DNAT --to-destination 192.168.2.2:1935
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 8301 -j DNAT --to-destination 192.168.2.2:8300
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 9001 -j DNAT --to-destination 192.168.2.2:9000
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 20080 -j DNAT --to-destination 192.168.2.2:8080
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 10022 -j DNAT --to-destination 192.168.2.2:22
```

