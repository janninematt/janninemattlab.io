---
title: "openface, dlib, opencv"
tags: [ "ML" ]
---

## install dlib

make sure your cmake version > 2.8

```
cmake --version
```

upgrade

```
sudo add-apt-repository ppa:george-edison55/cmake-3.x
sudo apt-get update
sudo apt-get install cmake
```

we need boostpython

```
sudo apt-get install libboost-all-dev
```

install dlib, it work in virtualenv too

```
pip install dlib
```

## install torch

before you install it, think one second

cuda support?

you must install cuda toolkit before install torch, that will auto config for that

```
git clone https://github.com/torch/distro.git ~/torch --recursive
cd ~/torch; bash install-deps;
./install.sh
```

with follow lib

```
for NAME in dpnn nn optim optnet csvigo cutorch cunn fblualib torchx tds; do luarocks install $NAME; done
```

## openface

Face recognition with deep neural networks


git clone and python setup.py install


edit `site-packages/openface/torch_neural_net.py`

line 130, 131 about the string and bytes convert

[refer](http://stackoverflow.com/questions/14010551/how-to-convert-between-bytes-and-strings-in-python-3)

```py
self.p.stdin.write((imgPath + "\n").encode(encoding='utf-8'))
output = self.p.stdout.readline().decode(encoding='utf-8')
```

## monitor gpu

here is monitor nividia gpu usage

```
watch -n 0.5 nvidia-smi
```

