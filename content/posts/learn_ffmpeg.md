---
title: "learn ffmpeg"
tags: [ "ffmpeg" ]
---

# History and Intro

以下是三個很早就開始開發的的大型程式。但之後三個互通有無，所以三個都有彼此的功能。

- FFmpeg 主攻影音轉檔
- Mplayer 主攻影音串流
- VLC 主攻影音播放

因為 FFmpeg 希望有容乃大，所以裡面盡量包山包海，再加上 FFmpeg 利用 Linux 的特性，使用「lib」函式庫的特性，讓使用者可以選擇需要用的編碼或解碼器自行編譯，製出符合自己的 FFmpeg。

FFmpeg全名是Fast Forward MPEG(Moving Picture Experts Group)，為開源的影音多媒體處理框架，可以進行影音的解碼、編碼、編碼轉換、混合、抽取、串流和濾鏡，無論影音格式是從哪個地方出來的，從過去到現在的影音格式它幾乎都能夠支援。FFmpeg也有很不錯的可攜性和可移植性，可以使用在Linux, Mac OS X, Microsoft Windows, BSDs, Solaris等多種不同的作業系統和多種硬體架構上。

FFmpeg提供給開發者和使用者絕佳的技術解決方案。除了自帶的函式庫之外，它還能夠和許多其它免費的第三方函式庫結合使用。開發者和使用者能夠自己選擇要啟用哪些函式庫，來建立出最適合自己用途的FFmpeg。

FFmpeg並不是一支簡單的小程式，它是個龐大的框架，由許多工具和函式庫組成。

工具包含以下幾種：

- ffmpeg：用來轉換不同格式的多媒體檔案，為FFmpeg最主要的工具。
- ffserver：可線上廣播影音的多媒體串流伺服器。
- ffplay：基於SDL(Simple DirectMedia Layer)和FFmpeg函式庫的簡易播放器。
- ffprobe：簡易的多媒體串流分析器。

函式庫則包含以下幾種：

- libavutil：一些程式上會用到的函式與資料結構。
- libavcodec：用來編碼與解碼不同的影音格式。
- libavformat：用來處理承載影音串流的多媒體容器。
- libavdevice：用來從不同的裝置輸入多媒體，或是將多媒體輸出至不同裝置。
- libavfilter：用來進行視訊和音訊的濾鏡處理。
- libswscale：用極佳的方式縮放影像，也可以用來轉換色彩空間。
- libswresample：用極佳的方式處理音訊取樣。
- 其它第三方函式庫：可藉由編譯FFmpeg前的configure程式來決定要開關哪些函式庫。如
    - lame
    - libxvid
    - libx264
    - libx265
    - libfdk-aac
    - libfaac

[取得FFmpeg](https://www.ffmpeg.org/)

取得原始碼之後，便可以在自己的環境上編譯FFmpeg了。如果您不知道該怎麼編譯FFmpeg，FFmpeg的官方網站也替不同的作業系統提供取得已編譯好的FFmpeg

compile ffmpeg 加入 cuda 硬體加速

```
CFLAGS='-I/usr/local/cuda-8.0/include' ./configure --prefix=/opt/ffmpeg-3.2.4 --enable-cuda --enable-nonfree
```

```bash
ffmpeg -vaapi_device /dev/dri/renderD128 -i input.mp4 -vf 'format=nv12,hwupload' -c:v h264_vaapi output.mp4
```


```
ffmpeg -i input.mpg output.mp4
```

什麼資訊都沒有加?
對， FFmpeg 會用預設的參數執行。在 -i 後面加上輸入檔，最後一個參數指定輸出檔，它會自動判斷檔名來使用不同的編碼器。

可是這不一定是我要的結果啊！
所以開始要加參數了嘛，如函式庫、轉檔要求等。

# CheatSheet

常見解析度

1. 240p (424x240, 0.10 megapixels)
1. 360p (640x360, 0.23 megapixels)
1. 432p (768x432, 0.33 megapixels)
1. 480p (848x480, 0.41 megapixels, "SD" or "NTSC widescreen")
1. 576p (1024x576, 0.59 megapixels, "PAL widescreen")
1. 720p (1280x720, 0.92 megapixels, "HD")
1. 1080p (1920x1080, 2.07 megapixels, "Full HD")


常見 ffmpeg 參數

```
-codecs    # list codecs
-c:v       # video codec (-vcodec) - 'copy' to copy stream
-c:a       # audio codec (-acodec)

-fs SIZE         # limit file size (bytes)

-b:v 1M          # video bitrate (1M = 1Mbit/s)
-b:a 1M          # audio bitrate

-aspect RATIO    # aspect ratio (4:3, 16:9, or 1.25)
-r RATE          # frame rate per sec
-s WIDTHxHEIGHT  # frame size
-vn              # no video

-aq QUALITY      # audio quality (codec-specific)
-ar 44100        # audio sample rate (hz)
-ac 1            # audio channels (1=mono, 2=stereo)
-an              # no audio
-vol N           # volume (256=normal)

-acodec
-vcodec
```

# Common command


## Example

任務 1

1. 將 gif 圖檔轉成數張 png
2. 再將 png 轉成 video mp4 format
3. video 轉成數張 jpg

![](/imgs/50.gif)


這邊可以抓 gif for CC license[gif CC](https://giphy.com/search/creative-commons)

圖片格式支援 :

1. PGM
2. PPM
3. PAM
4. PGMYUV
5. JPEG
6. GIF
7. PNG
8. TIFF
9. SGI

使用 convert 將 gif 轉成數張 png image

```bash
convert example.gif img/xx_%05d.png
```


Turn a X images to video

```bash
ffmpeg -f image2 -i image%d.png video.mp4
```


Turn a video to X images

```bash
ffmpeg -i video.mp4 image%d.jpg
```

## Example 2

影片 mp4 轉不同解析度 一路轉三路

1080p to 720, 480, 360p

範例影片 license CC - [Bachelor of Science in Engineering Sciences](/downloads/videos/example.mp4)

```bash
ffmpeg -i example.mp4  -s 1280x720 -c:v libx264 720p.mp4 \
    -s 858x480 -c:v libx264 480p.mp4 \
    -s 640x360 -c:v libx264 360p.mp4
```

## Example 3

提取影片 video, and audio only

### 從 source 提取 影片 to mp4

```bash
ffmpeg -i src.mp4 -an noaudio.mp4
```

`-an` is used to remove the audio from the video file

### 從 source 提取 聲音 to mp3

```
ffmpeg -i src.mp4 -vn -ab 128 novideo.mp3
```

- `-vn` is used to remove video
- `-ab` is used to set audio as 128Kbps


## Example 錄製螢幕

```
ffmpeg -video_size 1920x1080 -framerate 25 -f x11grab -i :0.0 -c:v libx264 output.mp4
```

## Example stream live to youtube

```
ffmpeg  -f x11grab -video_size 1280x720 -framerate 30 -i :0.0  -f alsa -i default -pix_fmt yuv444p -c:v libx264 -preset medium -b:v 2000k -bf 0 -bufsize 2000k -g 30 -strict -2 -c:a aac -ar 48000 -f flv rtmp://a.rtmp.youtube.com/live2/<key>
```

這邊特別注意 一定要在 source 中加入 audio, 否則 youtube 會擋



```
ffmpeg -i s_videofile.avi -vn -ar 44100 -ac 2 -ab 192 -f mp3 soundfile.mp3
```

- Source video : s_video.avi
- Audio bitrate : 192kb/s
- output format : mp3
- Generated sound : soundfile.mp3


```bash
ffmpeg -i source_video.avi input -acodec aac -ab 128kb -vcodec mpeg4 -b 1200kb -mbd 2 -flags +4mv+trell -aic 2 -cmp 2 -subcmp 2 -s 320x180 -title X final_video.mp4
```

- Source : source_video.avi
- Audio codec : aac
- Audio bitrate : 128kb/s
- Video codec : mpeg4
- Video bitrate : 1200kb/s
- Video size : 320px par 180px
- Generated video : final_video.mp4

# refer

- [FFmpeg 免費開源、功能強大的影音處理框架](https://magiclen.org/ffmpeg/)
- [19 ffmpeg commands for all needs](http://www.catswhocode.com/blog/19-ffmpeg-commands-for-all-needs)
- [FFmpeg 簡易教學](http://lnpcd.blogspot.tw/2012/09/ffmpeg.html)
- [HWAccelIntro](https://trac.ffmpeg.org/wiki/HWAccelIntro)
- [FFMPEG An Intermediate Guide/image sequence](https://en.wikibooks.org/wiki/FFMPEG_An_Intermediate_Guide/image_sequence)
- [digital_video_introduction](https://github.com/leandromoreira/digital_video_introduction)
- [技術專欄：線上影音播放的最大挑戰及因應(一)](http://www.onevision.com.tw/blog/index.php/01/12/%E6%8A%80%E8%A1%93%E5%B0%88%E6%AC%84%EF%BC%9A%E7%B7%9A%E4%B8%8A%E5%BD%B1%E9%9F%B3%E6%92%AD%E6%94%BE%E7%9A%84%E6%9C%80%E5%A4%A7%E6%8C%91%E6%88%B0%E5%8F%8A%E5%9B%A0%E6%87%89%E4%B8%80/)
- [ FFMPEG视音频编解码零基础学习方法 ](http://blog.csdn.net/leixiaohua1020/article/details/15811977)
