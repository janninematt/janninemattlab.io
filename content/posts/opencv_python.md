---
title: "Opencv with python example"
tags: [ "opencv", "python"]
---

## opencv python matplotlib show gray image intensity

```py
import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

img = cv2.imread('imgs/sample.jpg')

# resize it, make sure the pixel is not too large,it will consume alot resource
img = cv2.resize(img, (50, 50))

# convert the gray
gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
height, width = gray_image.shape

fig = plt.figure()
ax = Axes3D(fig)
X = np.arange(0, width, 1)
Y = np.arange(0, height, 1)
X, Y = np.meshgrid(X, Y)
# get each pixel intensity
R = gray_image.view()
ax.plot_surface(X, Y, R, rstride=1, cstride=1, cmap='rainbow')
plt.show()
```

origin

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Walters_Gallery.jpg/220px-Walters_Gallery.jpg">

[Imgur](http://i.imgur.com/6M7OOnA.png)

gray intensity

<a href="http://imgur.com/6M7OOnA"><img src="http://i.imgur.com/6M7OOnA.png" title="source: imgur.com" /></a>
## utility


```py
import cv2
import os
import numpy as np
import time
from threading import Thread


def draw_rectangle(image, coords):
    for (x, y, w, h) in coords:
        w_rm = int(0.2 * w / 2)
        cv2.rectangle(
            image, (x + w_rm, y), (x + w - w_rm, y + h), (150, 150, 0), 8)


def cut_faces(image, faces_coord):
    faces = []

    for (x, y, w, h) in faces_coord:
        w_rm = int(0.3 * w / 2)
        faces.append(image[y: y + h, x + w_rm: x + w - w_rm])

    return faces


def normalize_intensity(images):
    images_norm = []
    for image in images:
        is_color = len(image.shape) == 3
        if is_color:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        images_norm.append(cv2.equalizeHist(image))
    return images_norm


def resize(images, size=(50, 50)):
    images_norm = []
    for image in images:
        if image.shape < size:
            image_norm = cv2.resize(
                image, size, interpolation=cv2.INTER_AREA)
        else:
            image_norm = cv2.resize(
                image, size, interpolation=cv2.INTER_CUBIC)
        images_norm.append(image_norm)
    return images_norm


def normalize_faces(frame, faces_coord):
    faces = cut_faces(frame, faces_coord)
    faces = normalize_intensity(faces)
    faces = resize(faces)
    return faces


def collect_dataset(dir_path=None):
    if not os.path.isdir(dir_path):
        raise Exception('The dir path {} is not exist'.format(dir_path))

    images = []
    labels = []
    labels_dic = {}
    people = [person for person in os.listdir(dir_path)]
    for i, person in enumerate(people):
        labels_dic[i] = person
        for image in os.listdir(dir_path + person):
            images.append(cv2.imread(dir_path + person + '/' + image, 0))
            labels.append(i)
    return (images, np.array(labels), labels_dic)


class FaceDetector(object):

    def __init__(self, xml_path):
        self.classifier = cv2.CascadeClassifier(xml_path)

    def detect(self, image, biggest_only=True):
        scale_factor = 1.2
        min_neighbors = 5
        min_size = (30, 30)
        biggest_only = True
        flags = cv2.CASCADE_FIND_BIGGEST_OBJECT | \
            cv2.CASCADE_DO_ROUGH_SEARCH if biggest_only else \
            cv2.CASCADE_SCALE_IMAGE
        faces_coord = self.classifier.detectMultiScale(
            image, scaleFactor=scale_factor, minNeighbors=min_neighbors,
            minSize=min_size, flags=flags)
        return faces_coord


class Recorder(Thread):

    def __init__(self, video, writer, fps):
        super(Recorder, self).__init__()
        self.video = video
        self.writer = writer
        self.forward = True
        self.time_eclipse = 1 / fps
        self.frame = None

    def stop(self):
        if self.video is None:
            raise Exception('the video cam is not setup')
        print('stop Recorder')
        self.forward = False
        self.writer.release()

    def set_frame(self, frame):
        self.frame = frame

    def clear_frame(self):
        self.frame = None

    def save_frame(self):
        if self.frame is not None:
            self.writer.write(self.frame)
            # cv2.imshow('saving', self.frame)
        else:
            print('not writing')

    def run(self):
        if not self.video.isOpened():
            raise Exception('cam not open')
        while self.forward:
            if self.frame is not None:
                self.save_frame()
                self.clear_frame()


class VideoCamera(object):

    def __init__(self, index=0):
        self.video = cv2.VideoCapture(index)
        self.index = index
        self.writer = None
        print(self.video.isOpened())
        self.reocrd_thread = None

    def __del__(self):
        if self.writer is not None:
            self.end()
        self.video.release()

    def start_record(self):
        if self.reocrd_thread is None:
            raise Exception('The Recorder is not setup properly!')
        self.reocrd_thread.start()

    def stop_record(self):
        if self.reocrd_thread is None:
            raise Exception('The Recorder is not setup properly!')
        self.reocrd_thread.stop()

    def get_frame(self, in_grayscale=False):
        ret, frame = self.video.read()
        if in_grayscale:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        return ret, frame

    def get_size(self):
        width = int(self.video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(self.video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        return (width, height)

    def set_writer(self, codec, video_name, fps=20):
        codec = cv2.VideoWriter_fourcc(*codec)
        self.writer = cv2.VideoWriter(video_name, codec, fps, self.get_size(), 1)
        self.reocrd_thread = Recorder(writer=self.writer, fps=fps, video=self.video)

    def save(self, frame):
        if self.writer is None:
            raise Exception('VideoWriter is not setting properly')
        self.writer.write(frame)

    def end(self):
        if self.writer is None:
            raise Exception('VideoWriter is not setting properly')
        self.writer.release()

    def save_frame(self, frame):
        self.reocrd_thread.set_frame(frame=frame)
```


## photoshot

```py
import cv2
import os
from utilitys import VideoCamera, normalize_faces, draw_rectangle


webcam = VideoCamera(0)
folder = "people/" + input('Person: ').lower()  # input name
detector = cv2.CascadeClassifier("xml/frontal_face.xml")
cv2.namedWindow("Recording", cv2.WINDOW_AUTOSIZE)
if not os.path.exists(folder):
    os.mkdir(folder)
    counter = 1
    timer = 0
    while counter < 21:  # take 20 pictures
        ret, frame = webcam.get_frame()
        if ret:
            faces_coord = detector.detectMultiScale(frame)  # detect
            if len(faces_coord) and timer % 700 == 50:  # every Second or so
                faces = normalize_faces(frame, faces_coord)  # norm pipeline
                cv2.imwrite(folder + '/' + str(counter) + '.jpg', faces[0])
                # plt_show(faces[0], "Images Saved:" + str(counter))
                counter += 1
            draw_rectangle(frame, faces_coord)  # rectangle around face
            print(faces_coord)
            if len(faces_coord):
                cv2.putText(frame, 'take {} photo'.format(counter),
                            (faces_coord[0][0], faces_coord[0][1] - 10),
                            cv2.FONT_HERSHEY_PLAIN, 3, (66, 53, 243), 2)
            cv2.imshow("Recording", frame)  # live feed in external
            cv2.waitKey(10)
            timer += 50
    cv2.destroyAllWindows()
else:
    print("This name already exists.")
```

## recognizer

```py
import cv2
from utilitys import (VideoCamera, normalize_faces, draw_rectangle, collect_dataset)
import time
from pprint import pprint
photo_dir = 'people/'
filter_path = 'xml/frontal_face.xml'
record = False
video_path_name = 'output.avi'
codec = 'H264'
fps = 20
time_eclipse = 1 / fps


def read_model(photo_dir):
    images, labels, labels_dic = collect_dataset(photo_dir)
    rec_fisher = cv2.face.createFisherFaceRecognizer()
    rec_fisher.train(images, labels)
    return labels_dic, rec_fisher

if __name__ == '__main__':
    # start = time.time()

    labels_dic, models = read_model(photo_dir)
    detector = cv2.CascadeClassifier(filter_path)
    webcam = VideoCamera(0)

    if record:
        webcam.set_writer(codec, video_path_name, fps)
        webcam.start_record()
    current_time = time.time()
    while True:

        ret, frame = webcam.get_frame()
        if ret:
            faces_coord = detector.detectMultiScale(frame)
            # detect more than one face
            if len(faces_coord):
                faces = normalize_faces(frame, faces_coord)  # norm pipeline
                for i, face in enumerate(faces):  # for each detected face
                    pred, conf = models.predict(face)
                    threshold = 140
                    print("Prediction: " + labels_dic[pred].capitalize() +
                          "\nConfidence: " + str(round(conf)))
                    if conf <= threshold:
                        label = labels_dic[pred].capitalize()
                    else:
                        label = 'unknow'

                    cv2.putText(frame, label,
                                (faces_coord[i][0], faces_coord[i][1] - 10),
                                cv2.FONT_HERSHEY_PLAIN, 3, (66, 53, 243), 2)
                draw_rectangle(frame, faces_coord)  # rectangle around face

            cv2.putText(frame, "q to exit", (5, frame.shape[0] - 5),
                        cv2.FONT_HERSHEY_PLAIN, 1.3, (66, 53, 243), 2, cv2.LINE_AA)
            cv2.imshow("Demo", frame)  # live feed in external
            if record:
                webcam.save_frame(frame)
        # print('current_time {0}'.format(current_time - start))
        if cv2.waitKey(30) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break
    if record:
        webcam.stop_record()
    del webcam
```
#### Purge layer


```py
import cv2
import numpy as np
import matplotlib.pyplot as plt


def blank_three_channel_img(w, h):
    size = w, h, 3
    m = np.zeros(size, dtype=np.uint8)
    return m


def purge_ch_image(img, channels):
    '''
    img Mat
    channels (1, 0, 0) RGB if channel is 0, set 0 to img channel,
    this case will left Red color
    '''
    ## if direct use img, will refer to original, be careful
    tmp = img.copy()
    ## RGB
    for i, ch in enumerate(channels, 0):
        if ch == 0:
            tmp[:, :, i] = 0
    return tmp

## read image
img = cv2.imread('imgs/rgb.jpg')

## get property
## rows, columns and channels
width, height, channels = img.shape
print('width:{}, height:{}, channels:{}'.format(width, height, channels))
print('type img:', type(img))

## get location pixel info
print('img[100, 100]:', img[100, 100])

## plt.plot(img[100,:])

img_red = purge_ch_image(img, (1, 0, 0))
img_green = purge_ch_image(img, (0, 1, 0))
img_blue = purge_ch_image(img, (0, 0, 1))

## draw white line in middle of image
cv2.line(img_green, (0, int(height / 2)), (width, int(height / 2)), (255, 255, 255), 3)

t = range(width)

print(len(t))

## print(len(t), len(img_green[100, :]))
y = img_green[:, int(height / 2), 1]

print(len(y))

f, ((ax1, ax2, ax3), (ax4, ax5, _)) = plt.subplots(2, 3, sharey=True)
ax1.plot(t, y, 'g')
## ax1.set_xlim([0, width])
ax1.set_title('the green intensity(white line)')

ax2.imshow(img_green)
ax2.set_title('the green img')
ax3.imshow(img_blue)
ax3.set_title('the blue img')
ax4.imshow(img_red)
ax4.set_title('the red img')
ax5.imshow(img)
ax5.set_title('the original img')

plt.show()
cv2.destroyAllWindows()


## draw white line in middle of image
cv2.line(img_green, (0, int(height / 2)), (width, int(height / 2)), (255, 255, 255), 3)

t = range(width)
y = img_green[0:width, int(height / 2), 1]

f, ((ax1, ax2, ax3), (ax4, ax5, _)) = plt.subplots(2, 3, sharey=True)
ax1.plot(t, y, 'g')
ax1.set_xlim([0, width])
ax1.set_title('the green intensity(white line)')

ax2.imshow(img_green)
ax2.set_title('the green img')
ax3.imshow(img_blue)
ax3.set_title('the blue img')
ax4.imshow(img_red)
ax4.set_title('the red img')
ax5.imshow(img)
ax5.set_title('the original img')

plt.show()
cv2.destroyAllWindows()
```

<a href="http://imgur.com/L9UIjSA"><img src="http://i.imgur.com/L9UIjSA.png" title="source: imgur.com" /></a>


#### Predict

For OpenCV 3.1.0

```py
collector = cv2.face.MinDistancePredictCollector()
recognizer.predict(image, collector)
conf = collector.getDist()
pred = collector.getLabel()
```

For OpenCV 3.0.0

```py
prediction, confidence = recognizer.predict(image)
```
[Facial Recognition workflow](http://ccw1986.blogspot.tw/2013/01/knowledgeface-detection-recognition.html)

[Face Detection using OpenCV 2.X](http://ccw1986.blogspot.tw/2013/01/opencvface-detection.html)

[Face Recognition with OpenCV](http://docs.opencv.org/2.4/modules/contrib/doc/facerec/facerec_tutorial.html#id35)

[Cascade Classifier](http://docs.opencv.org/2.4/doc/tutorials/objdetect/cascade_classifier/cascade_classifier.html#cascade-classifier)

[目標檢測（Object Detection）原理與實現(五)](http://fanli7.net/a/bianchengyuyan/C__/20130626/375838.html)

[記錄, OpenCV 學習路徑, (3) 人臉辨識 (OpenCV, Python, Face Recognition)](http://gogoprivateryan.blogspot.tw/2015/09/opencv-3-opencv-python-face-recognition.html)

## haar

[ OpenCV学习笔记（三十三）——用haar特征训练自己的分类器（再做手势检测） ](http://blog.csdn.net/yang_xian521/article/details/7031030)
[淺析人臉檢測之Haar分類器方法：Haar特徵、積分圖、 AdaBoost 、級聯](http://alex-phd.blogspot.tw/2014/03/haarhaar-adaboost.html)


## recognize algorithm

1. [Eigenface](http://blog.csdn.net/smartempire/article/details/21406005)
2. [Fisherface（LDA)](http://blog.csdn.net/smartempire/article/details/23377385)
3. [LBP](http://blog.csdn.net/smartempire/article/details/23249517)

[Face Recognition with OpenCV](http://docs.opencv.org/2.4/modules/contrib/doc/facerec/facerec_tutorial.html#introduction)

[Haar Cascades vs. LBP Cascades in Face Detection](http://stackoverflow.com/questions/8791178/haar-cascades-vs-lbp-cascades-in-face-detection)

## must read

pdf [Robust Real-Time Face Detection](https://www.vision.caltech.edu/html-files/EE148-2005-Spring/pprs/viola04ijcv.pdf)

[[OpenCV] 人臉偵測 (Face Detection) ](http://ff320232.blogspot.tw/2013/06/opencv-face-detection.html)
[pdf](https://mega.nz/#!QIJTXLgD!Z_GLmHXXKnsQAi6k6NRswFZq3TgRzh3bMhVOnLx-CJ4)

[人脸识别必读的N篇文章](http://blog.csdn.net/smartempire/article/details/20462233)

## fun

[使用 OpenCV 識別圖片中的貓咪](https://read01.com/AE0ymj.html)

[Using Artificial Intelligence to Evaluate Handwritten Mathematical Expressions](http://www.willforfang.com/computer-vision/2016/4/9/artificial-intelligence-for-handwritten-mathematical-expression-evaluation)

[Digit Recognition using OpenCV, sklearn and Python](http://hanzratech.in/2015/02/24/handwritten-digit-recognition-using-opencv-sklearn-and-python.html)

[Simple Digit Recognition OCR in OpenCV-Python](http://stackoverflow.com/questions/9413216/simple-digit-recognition-ocr-in-opencv-python)

[vehicle_detection_haarcascades](https://github.com/andrewssobral/vehicle_detection_haarcascades)

## data

[AT&T The Database of Faces](http://www.cl.cam.ac.uk/research/dtg/attarchive/facedatabase.html)
[Yale Face Database](http://vision.ucsd.edu/content/yale-face-database)
[Extended Yale Facedatabase B](http://vision.ucsd.edu/~leekc/ExtYaleDatabase/ExtYaleB.html)

## cuda

[cuda-education-training](https://developer.nvidia.com/cuda-education-training)
[An Easy Introduction to CUDA C and C++](https://devblogs.nvidia.com/parallelforall/easy-introduction-cuda-c-and-c/)

## website

[opencvexamples](http://opencvexamples.blogspot.com/)
[pyimagesearch](http://www.pyimagesearch.com/)

