package main

import (
	"fmt"
	"net"
	"time"

	"github.com/pkg/errors"
)

func main() {
	res, err := sendUDP("127.0.0.1:8001", "hello")
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(res)
	}
}
func sendUDP(addr, msg string) (string, error) {

	conn, err := net.Dial("udp", addr)
	if err != nil {
		return "", errors.Wrapf(err, "could not connect to address:%s", addr)
	}

	// send to socket
	_, err = conn.Write([]byte(msg))
	if err != nil {
		return "", errors.Wrap(err, "could not write packet into socket")
	}

	// listen for reply
	bs := make([]byte, 1024)
	conn.SetDeadline(time.Now().Add(3 * time.Second))
	len, err := conn.Read(bs)
	if err != nil {
		return "", err
	}

	return string(bs[:len]), nil
}
