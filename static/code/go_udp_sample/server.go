package main

/*
用戶數據報協議（User Datagram Protocol，縮寫為UDP），又稱用戶數據報文協議，
是一個簡單的面向數據報(package-oriented)的傳輸層協議，正式規範為RFC 768。UDP只提供數據的不可靠傳遞，
它一旦把應用程序發給網絡層的數據發送出去，就不保留數據備份（所以UDP有時候也被認為是不可靠的數據報協議）。
UDP在IP數據報的頭部僅僅加入了復用和數據校驗。

由於缺乏可靠性且屬於非連接導向協議，UDP應用一般必須允許一定量的丟包、出錯和複製粘貼。但有些應用，比如TFTP，
如果需要則必須在應用層增加根本的可靠機制。但是絕大多數UDP應用都不需要可靠機制，甚至可能因為引入可靠機制而降低性能。
流媒體（流技術）、即時多媒體遊戲和IP電話（VoIP）一定就是典型的UDP應用。如果某個應用需要很高的可靠性，
那麼可以用傳輸控制協議（TCP協議）來代替UDP。

由於缺乏擁塞控制（congestion control），需要基於網絡的機制來減少因失控和高速UDP流量負荷而導致的擁塞崩潰效應。
換句話說，因為UDP發送者不能夠檢測擁塞，所以像使用包隊列和丟棄技術的路由器這樣的網絡基本設備往往就成為降低UDP過大
通信量的有效工具。數據報擁塞控制協議（DCCP）設計成通過在諸如流媒體類型的高速率UDP流中，
增加主機擁塞控制，來減小這個潛在的問題。


典型網絡上的眾多使用UDP協議的關鍵應用一定程度上是相似的。這些應用包括域名系統（DNS）、簡單網絡管理協議（SNMP）、
動態主機配置協議（DHCP）、路由信息協議（RIP）和某些影音流服務等等。
*/

import (
	"log"
	"net"
)

func main() {
	addr := "0.0.0.0:8001"
	listener, err := net.ListenPacket("udp", addr)
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	log.Printf("UDP server start and listening on %s.\n", addr)

	for {
		buf := make([]byte, 1024)
		// udp 無連接的協議
		n, remoteAddr, err := listener.ReadFrom(buf)
		if err != nil {
			log.Printf("ERR: coult not read from socket: address %s %v\n", remoteAddr, err)
			continue
		}

		go serve(listener, remoteAddr, buf[:n])

	}
}

func serve(listener net.PacketConn, addr net.Addr, buf []byte) {
	log.Printf("%s\t: %s\n", addr, buf)
	listener.WriteTo([]byte("message recived!\n"), addr)
}
